import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text } from 'react-native';
import { useState } from 'react';
import DropDownPicker from 'react-native-dropdown-picker';



export default function CuisinePicker({ cuisine, setCuisine }) {

    const [open, setOpen] = useState(false)





    const [items, setItems] = useState([
        { label: "All", value: "All" },
        { label: "American", value: "American" },
        { label: "Asian", value: "Asian" },
        { label: "British", value: "British" },
        { label: "Caribbean", value: "Caribbean" },
        { label: "Cental European", value: "Cental European" },
        { label: "Chinese", value: "Chinese" },
        { label: "Eastern European", value: "Eastern Europe" },
        { label: "French", value: "French" },
        { label: "Indian", value: "Indian" },
        { label: "Italian", value: "Italian" },
        { label: "Kosher", value: "Kosher" },
        { label: "Mediterranean", value: "Mediterranean" },
        { label: "Mexican", value: "Mexican" },
        { label: "Middle Eastern", value: "Middle Eastern" },
        { label: "Nordic", value: "Nordic" },
        { label: "South American", value: "South American" },
        { label: "South-East Asian", value: "South East Asian" }
    ])






    return (
        <View style={styles.cuisinepicker}>

            <Text
                style={styles.text}>Cuisine</Text>



            <DropDownPicker
                style={styles.picker}
                textStyle={{
                    fontFamily: "MontserratAlternates_400Regular"
                }}
                open={open}
                value={cuisine}
                items={items}
                setOpen={setOpen}
                setValue={setCuisine}
                dropDownDirection="TOP"
                dropDownContainerStyle={{
                    width: 150,
                    marginLeft: 55,
                }}
            />





        </View>
    );
}

const styles = StyleSheet.create({
    text: {
        width: 100,
        fontSize: 15,
        color: "white",
        fontWeight: "bold",
        textAlign: 'left',
        fontFamily: "MontserratAlternates_800ExtraBold"
    },
    picker: {
        color: 'black',
        width: 150,
        marginLeft: 55,
        marginRight: 75,

    },
    cuisinepicker: {
        width: 300,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginBottom: 30,
        margin: 3,
        // marginTop: 30,

    },


});


