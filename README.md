# Use It Up - A React native App for iOS and Android

This app was built using React Native and React Native Async Storage. Libraries used were React Native Elements and React Native Paper. 

This app uses Edamam, a public recipe search API. I integrated filters into the UI to extract relevant data from the API database and enhance user experience. 

## How to run locally

Clone the repository, then in the terminal: 

1) cd use-it-up-react-native-app
2) npm install
3) expo start

You should now be able to access the application, search for recipes using the search engine, see results and visit recipe links and save desired recipes to your personalised list. 