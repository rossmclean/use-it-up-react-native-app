import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, Image, ScrollView, TouchableOpacity, BackHandler } from 'react-native';
import { Card, Icon, Button } from '@rneui/themed';







export default function Home({ setDisplaySearch, displaySearch }) {



    return (
        <View style={styles.container}>

            <ScrollView>
                




                <View style={{ flexDirection: 'column', marginVertical: 20, alignItems: "center" }}>





                    <View style={{ flexDirection: "column", borderRadius: 20, backgroundColor: "#07BEB8", width: "90%" }}>

                        <Icon
                            name="kitchen"
                            type="material"
                            color="white"
                            size= {200}
                            style={{ marginVertical: 5, textAlign: "left"}}></Icon>

                        <View style={{ flexDirection: "row", marginVertical: 0, alignItems: "center", justifyContent: "center" }}>
                            <Text style={{ fontSize: 60, color: "white", fontWeight: "bold", textAlign: "center", fontFamily: "MontserratAlternates_800ExtraBold_Italic" }}>Use</Text>
                            <Text style={{ fontSize: 60, color: "black", fontWeight: "bold", textAlign: "center", fontFamily: "MontserratAlternates_800ExtraBold_Italic" }}>It</Text>
                            <Text style={{ fontSize: 60, color: "white", fontWeight: "bold", textAlign: "center", fontFamily: "MontserratAlternates_800ExtraBold_Italic" }}>Up</Text>

                        </View>



                    </View>



                    <Card containerStyle={{ marginTop: 15, alignItems: "center", borderRadius: 20, marginBottom: 0 }} wrapperStyle={{ color: "red" }}>
                        <Card.Title style={{ color: "black", fontSize: 25, fontWeight: "bold", fontFamily: "MontserratAlternates_600SemiBold_Italic" }}>How it works</Card.Title>
                        <Card.Divider />


                        <Icon

                            size={40}
                            name='plus-circle'
                            type='font-awesome-5'
                            color='#07BEB8'
                        />
                        <Text style={styles.stepstext}>1. Add up to 3 ingredients that you need to use up. Filter for diet, type of cuisine and meal time </Text>

                        <Icon

                            size={40}
                            name='eye'
                            type='font-awesome-5'
                            color='#07BEB8'
                        />
                        <Text style={styles.stepstext}>2. View your curated list of 20 recipes from the web, and sort to suit your ingredient availability and meal preferences using our built-in filters</Text>

                        <Icon

                            size={40}
                            name='grin-hearts'
                            type='font-awesome-5'
                            color='#07BEB8'
                        />
                        <Text style={styles.stepstext}>3. Save your favourite recipes to My List for later, while saving money and unnecessary trips to the supermarket</Text>



                    </Card>


                </View>


                <View style={{ alignItems: 'center', borderRadius: 20, backgroundColor: "white", marginBottom: 50, width: "90%", marginHorizontal: "5%" }}>
                    <Button
                        title="GO!"
                        titleStyle={{ fontWeight: 'bold', fontSize: 25, fontFamily: "MontserratAlternates_600SemiBold_Italic" }}

                        buttonStyle={{
                            backgroundColor: "#07BEB8",
                            borderWidth: 0,
                            borderColor: 'transparent',
                            borderRadius: 20,
                        }}
                        containerStyle={{
                            width: 250,
                            marginHorizontal: 50,
                            marginTop: 20,
                            marginBottom: 20,
                        }}
                        icon={{
                            name: 'arrow-right',
                            type: 'font-awesome',
                            size: 25,
                            color: 'black',
                        }}
                        iconRight
                        iconContainerStyle={{ marginLeft: 10, marginRight: -10 }}
                        onPress={() => setDisplaySearch(true)} />



                </View>

            </ScrollView>





        </View>



    )
}

const styles = StyleSheet.create({
    titletext: {
        textAlign: "center",
        marginTop: 40,
        marginBottom: 40,
        fontSize: 60,
        color: "#2E67F8",
    },
    stepstext: {
        fontFamily: "MontserratAlternates_400Regular",
        color: "black",
        fontSize: 15,
        margin: 15,
        paddingLeft: 20,
        paddingRight: 20,
        textAlign: 'left',
        borderRadius: 20
    },
    container: {
        flex: 1,

    },
    mainimg: {
        borderWidth: 10,
        borderColor: "white",
        borderRadius: 20,
        marginTop: 100,
        height: 400,
        width: 400,

    },
    btn: {
        alignItems: 'center',
        width: 250,
        marginTop: 30,
        backgroundColor: "white",
    }


})