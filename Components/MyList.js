import React, { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Text, ActivityIndicator } from 'react-native';
import { Chip } from '@rneui/themed'
import MyListItems from './MyListItems';


export default function MyList({
    setSaved,
    loading,
    isError,
    saved,
    handleSave,
    meal,
    diet,
    cuisine,
    _storeData }) {

    const handleSortNumber = () => {
        let lowHigh = saved.sort((a, b) => a.recipe.ingredients.length - b.recipe.ingredients.length)
        setSaved([...lowHigh])
    }

    const handleSortYield = () => {
        let yieldItems = saved.sort((a, b) => a.recipe.yield - b.recipe.yield)
        setSaved([...yieldItems])
    }

    const handleSortTime = () => {
        let sortTime = saved.sort((a, b) => a.recipe.totalTime - b.recipe.totalTime)
        setSaved([...sortTime])
    }



    return (
        <View style={{ alignItems: "center", width: "100%", flex: 5 }}>

            <Text style={styles.title}>My List</Text>



            <View style={{ alignItems: "center" }}>


                <Text style={styles.iteminfo}>Sort by:</Text>
                <Chip
                    icon={{
                        name: "shopping-basket",
                        type: "font-awesome-5",
                        color: "black",
                        size: 15,
                    }}
                    onPress={() => handleSortNumber()}
                    style={styles.filterchip}
                    buttonStyle={{ backgroundColor: "white" }}
                    titleStyle={{ color: "black" }}

                > <Text style={{ fontSize: 12, textAlign: "center" }}>Num. of ingredients (lo-hi)</Text></Chip>

                <Chip

                    icon={{
                        name: "hourglass",
                        type: "font-awesome-5",
                        color: "black",
                        size: 15,
                    }}
                    onPress={() => handleSortTime()}
                    style={styles.filterchip}
                    buttonStyle={{ backgroundColor: "white" }}
                    titleStyle={{ color: "black" }}

                > <Text style={{ fontSize: 12, textAlign: "left" }}>Cooking Time</Text></Chip>

                <Chip
                    icon={{
                        name: "users",
                        type: "font-awesome-5",
                        color: "black",
                        size: 15,
                    }}
                    onPress={() => handleSortYield()}
                    style={styles.filterchip}
                    buttonStyle={{ backgroundColor: "white" }}
                    titleStyle={{ color: "black" }}

                > <Text style={{ fontSize: 12, textAlign: "left" }}>Yield</Text></Chip>

            </View>


            {loading ? <ActivityIndicator size="large" color="white" /> : null}
            {isError && <Text>{isError}</Text>}

            {saved.length === 0 ? <Text style={styles.title}>Your list is empty! :(</Text> : <FlatList
                style={{ backgroundColor: "white", width: "100%", flex: 5 }}

                data={saved}
                renderItem={({ item }) => <MyListItems diet={diet} setSaved={setSaved} saved={saved} handleSave={handleSave} item={item} meal={meal} cuisine={cuisine} title={item.recipe.label} img={item.recipe.images.REGULAR.url} _storeData={_storeData} />}
            />}
            



        </View>
    )
}

const styles = StyleSheet.create({

    filterchip: {
        alignItems: "left",
        marginBottom: 10,

    },

    title: {
        fontSize: 40,
        marginVertical: 30,
        color: "white",
        fontFamily: "MontserratAlternates_800ExtraBold_Italic",
        textAlign: "center"
    },
    iteminfo: {
        color: "white",
        fontFamily: "MontserratAlternates_400Regular",
        textAlign: "left",
        fontSize: 20,
        marginBottom: 5
    }
})