import React from 'react';
import { useState } from 'react';
import {  RefreshControl , View, ScrollView, StyleSheet, Text, Image, Linking } from 'react-native';
import { Chip } from "@rneui/themed";




export default function ListItems({ item, title, img, meal, cuisine, diet, saved, handleSave }) {

  


    return (
        <>
       
        <View style={styles.item}>
            

            <View style={styles.imgAndTitle}>

                <Image
                    style={styles.smallimg}
                    source={{ uri: img }}
                />
                <Text style={styles.title} onPress={() => Linking.openURL(`${item.recipe.url}`)} >{title}</Text>

            </View>



            <View style={{ flexDirection: "row", alignItems: 'center', justifyContent: "space-between", paddingTop: 15 }}>
                <View style={{ flexDirection: "column", alignItems: 'center', width: "35%" }}>


                    <Chip
                        title={meal}
                        type="outline"
                        color="white"
                        size="sm"
                        titleStyle={{
                            color: 'white',
                            textTransform: 'capitalize',
                            marginHorizontal: 3
                        }}
                        icon={{
                            name: 'utensils',
                            type: 'font-awesome-5',
                            size: 15,
                            color: 'white',
                        }}
                        containerStyle={{ marginVertical: 7 }}
                    />
                    <Chip
                        title={cuisine}
                        type="outline"
                        color="white"
                        size="sm"
                        titleStyle={{
                            color: 'white',
                            textTransform: 'capitalize',
                            marginHorizontal: 3
                        }}
                        icon={{
                            name: 'globe',
                            type: 'font-awesome',
                            size: 15,
                            color: 'white',
                        }}
                        containerStyle={{ marginVertical: 7 }}
                    />

                    <Chip
                        title={diet}
                        type="outline"
                        color="white"
                        size="sm"
                        titleStyle={{
                            color: 'white',
                            textTransform: 'capitalize',
                            marginHorizontal: 3
                        }}
                        icon={{
                            name: 'heartbeat',
                            type: 'font-awesome',
                            size: 15,
                            color: 'white',
                        }}
                        containerStyle={{ marginVertical: 7 }}
                    />




                </View>

                <View style={{ flexDirection: "column", marginLeft: "15%" }}>
                    <Text style={styles.iteminfo}>Number of ingredients: {item.recipe.ingredients.length}</Text>
                    <Text style={styles.iteminfo}>Yield: {item.recipe.yield}</Text>
                    <Text style={styles.iteminfo}>Cooking time: {item.recipe.totalTime} mins</Text>


                    <View style={{ marginVertical: 10 }}>
                        <Chip
                            title={saved.includes(item) ? "Saved!" : "Save to My List"}
                            type="solid"
                            color="white"
                            size="md"
                            titleStyle={{
                                color: '#07BEB8',
                                fontFamily: "MontserratAlternates_800ExtraBold",
                                textTransform: 'capitalize',
                                marginHorizontal: 3
                            }}
                            icon={saved.includes(item) ? {
                                name: 'heart',
                                type: 'font-awesome-5',
                                size: 15,
                                color: '#07BEB8',
                            } : {
                                name: "grin-hearts",
                                type: 'font-awesome-5',
                                size: 15,
                                color: '#07BEB8',

                            }}
                            onPress={() => handleSave(item)}
                            containerStyle={{ marginVertical: 5 }}
                        />
                    </View>
                </View>


            </View>



        </View>
        </>
    )
}

const styles = StyleSheet.create({
    imgAndTitle: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 5
    },
    item: {
        width: '100%',
        flexDirection: "column",
        alignItems: 'left',
        backgroundColor: '#07BEB8',
        padding: 5,
        marginVertical: 10,
        borderRadius: 10,
        borderWidth: 5,
        borderColor: "white"
    },
    title: {
        width: "50%",
        fontSize: 15,
        padding: 0,
        marginLeft: 10,
        color: "white",
        textAlign: "center",
        textTransform: "capitalize",
        fontFamily: "MontserratAlternates_800ExtraBold",


    },
    smallimg: {
        width: 150,
        height: 120,
        borderRadius: 40,
        borderWidth: 3,
        borderColor: "white"
    },
    iteminfo: {
        color: "white",
        fontFamily: "MontserratAlternates_400Regular",
        textAlign: "right",
    },
 

});