import React from 'react';
import { useState } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text } from 'react-native';


import { Icon, Chip, Button } from '@rneui/themed'


export default function Navchips({ setDisplaySearch, setDisplayResults, setDisplayList, setIngredientOne, setIngredientTwo, setIngredientThree }) {

    let handleHome = () => {
        setDisplayResults(false)
        setDisplaySearch(false)
        setDisplayList(false)
    }

    let handleSearch = () => {
        setDisplaySearch(true)
        setDisplayResults(false)
        setDisplayList(false)
        setIngredientOne("")
        setIngredientTwo("")
        setIngredientThree("")
        
    }

    let handleList = () => {
        setDisplayList(true)
        setDisplayResults(false)
        setDisplaySearch(false)
    }

    let handleResults = () => {
        setDisplayList(false)
        setDisplayResults(true)
        setDisplaySearch(false)
    }











    return (
        <View style={{ flexDirection: "row", position: "fixed", paddingVertical: 10, backgroundColor: "whitesmoke" }}>

            <Chip
                title="Home"
                icon={{
                    name: 'home',
                    type: 'font-awesome-5',
                    size: 20,
                    color: 'black',
                }}
                onPress={handleHome}
                iconRight
                containerStyle={{ marginVertical: 10, width: "25%", backgroundColor: "white" }}
                buttonStyle={{ backgroundColor: "white" }}
                titleStyle={{ color: "black", fontFamily: "MontserratAlternates_800ExtraBold" }}

            />

            <Chip
                title="Search"
                icon={{
                    name: 'eye',
                    type: 'font-awesome-5',
                    size: 20,
                    color: 'black',
                }}
                onPress={handleSearch}
                iconRight
                type="solid"
                containerStyle={{ marginVertical: 10, width: "25%" }}
                buttonStyle={{ backgroundColor: "white" }}
                titleStyle={{ color: "black", fontFamily: "MontserratAlternates_800ExtraBold" }}
            />

            <Chip
                title="Results"
                icon={{
                    name: 'clipboard-list',
                    type: 'font-awesome-5',
                    size: 20,
                    color: 'black',
                }}
                onPress={handleResults}
                iconRight
                type="solid"
                containerStyle={{ marginVertical: 10, width: "25%" }}
                buttonStyle={{ backgroundColor: "white" }}
                titleStyle={{ color: "black", fontFamily: "MontserratAlternates_800ExtraBold" }}
            />

            <Chip
                title="My List"
                icon={{
                    name: 'grin-hearts',
                    type: 'font-awesome-5',
                    size: 20,
                    color: 'black',
                }}
                onPress={handleList}
                iconRight
                type="solid"
                containerStyle={{ marginVertical: 10, width: "25%" }}
                buttonStyle={{ backgroundColor: "white" }}
                titleStyle={{
                    color: "black", fontFamily: "MontserratAlternates_800ExtraBold",
                }}
            />










        </View>
    );
}

const styles = StyleSheet.create({



});