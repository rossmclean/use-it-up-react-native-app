import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, ActivityIndicator, ScrollView, RefreshControl } from 'react-native';
import { Icon, Chip, Button } from '@rneui/themed'
import ListItems from './ListItems';


export default function Results({ setDisplayResults,
    handleSortNumber,
    handleSortYield,
    handleSortTime,
    loading,
    isError,
    data,
    saved,
    handleSave,
    meal,
    diet,
    cuisine,
    fetchData,
    ingredientOne,
    refreshResults }) {


    const wait = (timeout) => {
        refreshResults()
        return new Promise(resolve => setTimeout(resolve, timeout));
    }

    const [refreshing, setRefreshing] = useState(false)

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        wait(1000).then(() => setRefreshing(false));
    }, []);




    return (
        <View style={{ alignItems: "center", width: "100%", flex: 5 }}>



            <Text style={styles.title}>Results</Text>
            <Text style={styles.iteminfo}>Sort by:</Text>
            <View style={{ alignItems: "center" }}>

                <Chip
                    icon={{
                        name: "shopping-basket",
                        type: "font-awesome-5",
                        color: "black",
                        size: 15,
                    }}
                    onPress={() => handleSortNumber()}
                    style={styles.filterchip}
                    buttonStyle={{ backgroundColor: "white" }}
                    titleStyle={{ color: "black" }}

                > <Text style={{ fontSize: 12, textAlign: "left" }}>Ingredients (lo-hi)</Text></Chip>

                <Chip

                    icon={{
                        name: "hourglass",
                        type: "font-awesome-5",
                        color: "black",
                        size: 15,
                    }}
                    onPress={() => handleSortTime()}
                    style={styles.filterchip}
                    buttonStyle={{ backgroundColor: "white" }}
                    titleStyle={{ color: "black" }}

                > <Text style={{ fontSize: 12, textAlign: "left" }}>Cooking Time</Text></Chip>

                <Chip
                    icon={{
                        name: "users",
                        type: "font-awesome-5",
                        color: "black",
                        size: 15,
                    }}
                    onPress={() => handleSortYield()}
                    style={styles.filterchip}
                    buttonStyle={{ backgroundColor: "white" }}
                    titleStyle={{ color: "black" }}

                > <Text style={{ fontSize: 12, textAlign: "left" }}>Yield</Text></Chip>

            </View>
            

            {isError && <Text>{isError}</Text>}

            {data.length === 0 && !loading ? <View><Text style={styles.infomsg}></Text>

                <Text style={styles.infomsg}>Touch Search button below to begin</Text></View> : <>
                
                {!loading && <Text onPress={()=>onRefresh()} style={{marginTop: 10, color: "white", fontFamily: "MontserratAlternates_400Regular", fontSize: 20}}>Tap to refresh results</Text>}

                {loading ? <View style={{marginTop: 100}}><ActivityIndicator size="large" color="white" /><Text style={{marginTop: 10, color: "white", fontFamily: "MontserratAlternates_400Regular",}}>Getting new recipes...</Text></View> : <View style={{flex: 5}}>
                    

                    <FlatList
                        style={{ backgroundColor: "#07BEB8", width: "100%", flex: 5 }}
                        data={data}
                        renderItem={({ item }) => <ListItems diet={diet} saved={saved} handleSave={handleSave} item={item} meal={meal} cuisine={cuisine} title={item.recipe.label} img={item.recipe.images.REGULAR.url} />}></FlatList>
                    </View>}
                
                </>
            }



        </View>
    )
}

const styles = StyleSheet.create({
    button: {
        height: 40,
        backgroundColor: "white",
        alignItems: "center",
        justifyContent: "center",
        textTransform: "none",
        paddingLeft: 5,
        paddingRight: 5,
        width: 120,
        marginTop: 10,
        marginBottom: 8

    },
    filterchip: {
        alignItems: "left",
        marginBottom: 10,

    },
    title: {
        fontSize: 40,
        marginVertical: 30,
        color: "white",
        fontFamily: "MontserratAlternates_800ExtraBold_Italic"
    },
    infomsg: {
        fontSize: 30,
        marginVertical: 20,
        marginHorizontal: 20,
        color: "white",
        fontFamily: "MontserratAlternates_800ExtraBold_Italic",
        textAlign: "center"
    },
    iteminfo: {
        color: "white",
        fontFamily: "MontserratAlternates_400Regular",
        textAlign: "left",
        fontSize: 20,
        marginBottom: 5
    },
    scrollView: {
        backgroundColor: 'pink',
        alignItems: 'center',
        justifyContent: 'center',
    },
})