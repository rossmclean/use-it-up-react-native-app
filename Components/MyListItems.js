import React, {useEffect} from 'react';
import { Linking, View, FlatList, StyleSheet, Text, Image } from 'react-native';
import { Chip } from "@rneui/themed";




export default function MyListItems({ item, title, img, saved, setSaved, _storeData }) {


    const handleRemove = async (item) => {
        console.log(item)
        let index = saved.indexOf(item)
        saved.splice(index, 1)
        setSaved([...saved])
        console.log('data updated')
        await _storeData(saved)
        // try {
        //     await AsyncStorage.removeItem(item);
        //     console.log('item deleted');
        // }
        // catch(exception) {
        //     console.log('failed');
        // }
    
        

    }



    return (
        <View style={styles.item}>

            <View style={styles.imgAndTitle}>

                <Image
                    style={styles.smallimg}
                    source={{ uri: img }}
                />
                <Text style={styles.title} onPress={() => Linking.openURL(`${item.recipe.url}`)}>{title}</Text>
            </View>



            <View style={{ flexDirection: "row", alignItems: 'center', paddingTop: 15, width: "90%" }}>
                <View style={{ flexDirection: "column", alignItems: 'left', width: "35%" }}>


                    <Chip
                        title={item.recipe.mealType}
                        type="outline"
                        color="white"
                        size="sm"
                        titleStyle={{
                            color: 'white',
                            textTransform: 'capitalize',
                            marginHorizontal: 0
                        }}
                        icon={{
                            name: 'utensils',
                            type: 'font-awesome-5',
                            size: 15,
                            color: 'white',
                        }}
                        containerStyle={{ marginVertical: 7 }}
                    />
                    <Chip
                        title={item.recipe.cuisineType[0]}
                        type="outline"
                        color="white"
                        size="sm"
                        titleStyle={{
                            color: 'white',
                            textTransform: 'capitalize',
                            marginHorizontal: 0
                        }}
                        icon={{
                            name: 'globe',
                            type: 'font-awesome',
                            size: 15,
                            color: 'white',
                        }}
                        containerStyle={{ marginVertical: 7 }}
                    />






                </View>

                <View style={{ flexDirection: "column", marginLeft: "10%"}}>
                    <Text style={styles.iteminfo}>Number of ingredients: {item.recipe.ingredients.length}</Text>
                    <Text style={styles.iteminfo}>Yield: {item.recipe.yield}</Text>
                    <Text style={styles.iteminfo}>Cooking time: {item.recipe.totalTime} mins</Text>


                    <View style={{ marginLeft: 10, marginRight: 0 }}>

                        <Chip
                            title="Remove from My List"
                            type="solid"
                            color="white"
                            size="md"
                            titleStyle={{
                                color: '#07BEB8',
                                fontFamily: "MontserratAlternates_800ExtraBold",
                                textTransform: 'capitalize',
                                marginHorizontal: 0
                            }}
                            icon={saved.includes(item) ? {
                                name: 'trash-alt',
                                type: 'font-awesome-5',
                                size: 15,
                                color: '#07BEB8',
                            } : {
                                name: "grin-hearts",
                                type: 'font-awesome-5',
                                size: 15,
                                color: '#07BEB8',

                            }}
                            onPress={() => handleRemove(item)}
                            containerStyle={{ marginVertical: 5 }}
                        />

                    </View>
                </View>


            </View>



        </View>
    )
}

const styles = StyleSheet.create({
    imgAndTitle: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 5
    },
    item: {
        width: '100%',
        flexDirection: "column",
        alignItems: 'left',
        backgroundColor: '#07BEB8',
        padding: 5,
        marginVertical: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "white"
    },
    title: {
        width: "50%",
        fontSize: 15,
        padding: 0,
        marginLeft: 10,
        color: "white",
        textAlign: "center",
        textTransform: "capitalize",
        fontFamily: "MontserratAlternates_800ExtraBold",


    },
    smallimg: {
        width: 150,
        height: 120,
        borderRadius: 40,
        borderWidth: 3,
        borderColor: "white"
    },
    iteminfo: {
        color: "white",
        fontFamily: "MontserratAlternates_400Regular",
        textAlign: "right",


    }


});