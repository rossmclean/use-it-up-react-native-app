import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, TouchableOpacity, TextInput } from 'react-native';
import { Button, Chip, Icon } from '@rneui/themed';
import { ScrollView } from 'react-native';
import MealPicker from './MealPicker';
import CuisinePicker from './CuisinePicker';
import DietPicker from './DietPicker';
import ListItems from './ListItems';



export default function SearchEngine({
    setIngredientOne,
    setIngredientTwo,
    setIngredientThree,
    ingredientOne,
    ingredientTwo,
    ingredientThree,
    count,
    setCount,
    meal,
    setMeal,
    cuisine,
    setCuisine,
    handleSubmit,
    diet,
    setDiet, }) {

   

 

       




    return (
        <ScrollView keyboardDismissMode='on-drag' showsVerticalScrollIndicator={false}  >


            

            <View style={{ alignItems: "center", margin: 5 }}>

                
                


                <Text style={styles.title}>Search</Text>

                <View style={{ marginBottom: 50, justifyContent: "center" }}>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                        <TextInput
                            style={styles.input}
                            onChangeText={setIngredientOne}
                            value={ingredientOne}
                            placeholder="Mushroom, Dill..."

                        />
                        <View style={{ alignItems: "center" }}>
                            <TouchableOpacity
                                style={styles.addremovebutton}
                                activeOpacity={0.7}
                                onPress={() => setCount(count + 1)}
                            >


                                <Icon
                                    name='plus-circle'
                                    type='font-awesome-5'
                                    color='#07BEB8' />

                            </TouchableOpacity>



                        </View>



                    </View>

                    {count >= 2 ?
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TextInput
                                style={styles.input}
                                onChangeText={setIngredientTwo}
                                value={ingredientTwo}
                                placeholder="Banana, Chocolate..."
                            />
                            <View style={{ alignItems: "center" }}>
                                <TouchableOpacity
                                    style={styles.addremovebutton}
                                    activeOpacity={0.7}
                                    onPress={() => setCount(count + 1)}
                                >
                                    <Icon
                                        name='plus-circle'
                                        type='font-awesome-5'
                                        color='#07BEB8' />

                                </TouchableOpacity>
                            </View>

                            <View style={{ alignItems: "center" }}>
                                <TouchableOpacity
                                    style={styles.addremovebutton}
                                    activeOpacity={0.7}
                                    onPress={() => { setCount(count - 1); setIngredientTwo("") }}
                                >
                                    <Icon
                                        name='minus-circle'
                                        type='font-awesome-5'
                                        color='#07BEB8' />

                                </TouchableOpacity>
                            </View>


                        </View> :
                        null}


                    {count == 3 ?

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TextInput
                                style={styles.input}
                                onChangeText={setIngredientThree}
                                value={ingredientThree}
                                placeholder="Pork, Oregano..."
                            />
                            <View style={{ alignItems: "center" }}>
                                <TouchableOpacity
                                    style={styles.addremovebutton}
                                    activeOpacity={0.7}
                                    onPress={() => setCount(count + 1)}
                                >
                                    <Icon
                                        name='plus-circle'
                                        type='font-awesome-5'
                                        color='#07BEB8' />

                                </TouchableOpacity>
                            </View>

                            <View style={{ alignItems: "center" }}>
                                <TouchableOpacity
                                    style={styles.addremovebutton}
                                    activeOpacity={0.7}
                                    onPress={() => { setCount(count - 1); setIngredientThree("") }}
                                >
                                    <Icon
                                        name='minus-circle'
                                        type='font-awesome-5'
                                        color='#07BEB8' />

                                </TouchableOpacity>
                            </View>


                        </View> :
                        null}

                </View>


                <MealPicker meal={meal} setMeal={setMeal} />

                <CuisinePicker cuisine={cuisine} setCuisine={setCuisine}></CuisinePicker>

                <DietPicker diet={diet} setDiet={setDiet} />

                {ingredientOne.length > 2 && <View style={{ marginBottom: 50, width: "100%" }}>
                    <Button
                        title="Search!"
                        titleStyle={{ fontWeight: 'bold', fontSize: 25, fontFamily: "MontserratAlternates_600SemiBold_Italic", color: "#07BEB8" }}

                        buttonStyle={{
                            backgroundColor: "white",
                            borderWidth: 0,
                            borderColor: 'transparent',
                            borderRadius: 20,
                        }}
                        containerStyle={{
                            width: 250,
                            marginHorizontal: 50,
                            marginTop: 20,
                            marginBottom: 20,
                        }}
                        icon={{
                            name: 'arrow-right',
                            type: 'font-awesome',
                            size: 25,
                            color: 'black',
                        }}
                        iconRight
                        iconContainerStyle={{ marginLeft: 10, marginRight: -10 }}
                        onPress={handleSubmit} />

                </View>}


                
            </View>
            </ScrollView>
       


    )
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        alignItems: 'center',
    },
    item: {
        width: '90%',
        flexDirection: "column",
        alignItems: 'center',
        borderWidth: 2,
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 16,
        marginHorizontal: 10,
    },
    title: {
        fontSize: 40,
        marginVertical: 30,
        color: "white",
        fontFamily: "MontserratAlternates_800ExtraBold_Italic"


    },
    input: {
        backgroundColor: "white",
        height: 40,
        width: 200,
        margin: 12,
        padding: 10,
        borderRadius: 10,
    },
    smallimg: {
        width: 150,
        height: 120
    },
    text: {
        fontSize: 10,
        color: "white",
        fontWeight: "bold",
        textAlign: 'center',
    },
    button: {
        width: 100,
        padding: 20,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: 'white',
    },
    addremovebutton: {
        marginRight: 10,
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        borderRadius: 100,
        backgroundColor: 'white',
    },
    types: {
        borderRadius: 15,
        padding: 5,
        margin: 5,
        backgroundColor: "white",
        fontSize: 12,
        color: 'black',


    }
});