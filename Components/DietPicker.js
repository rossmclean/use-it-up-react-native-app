import React from 'react';
import { useState } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';



export default function DietPicker({ diet, setDiet }) {

    const [open, setOpen] = useState(false)
    const [items, setItems] = useState([
        { label: "All", value: "All" },
        { label: "Gluten Free", value: "gluten-free" },
        { label: "Vegan", value: "vegan" },
        { label: "Vegetarian", value: "vegetarian" },
        { label: "Low Sugar", value: "low-sugar" },
    ])










    return (
        <View style={styles.mealpicker}>

            <Text
                style={styles.text}>Diet</Text>



            <DropDownPicker
                style={styles.picker}
                textStyle={{
                    fontFamily: "MontserratAlternates_400Regular"
                }}
                open={open}
                value={diet}
                items={items}
                setOpen={setOpen}
                setValue={setDiet}
                setItems={setItems}
                dropDownDirection="TOP"
                dropDownContainerStyle={{
                    width: 120,
                    marginLeft: 55,
                }}



            />


        </View>
    );
}

const styles = StyleSheet.create({
    text: {
        width: 100,
        fontSize: 15,
        color: "white",
        fontWeight: "bold",
        textAlign: 'left',
        fontFamily: "MontserratAlternates_800ExtraBold"

    },
    mealpicker: {
        width: 300,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginBottom: 30,
        margin: 3,

    },
    picker: {

        color: 'black',
        width: 150,
        marginLeft: 55,
        marginRight: 75,

    },


});