import React from 'react';
import { useState } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { ScrollView } from 'react-native-web';


export default function MealPicker({ meal, setMeal }) {

  const [open, setOpen] = useState(false)
  const [items, setItems] = useState([
    { label: "All", value: "All" },
    { label: "Breakfast", value: "Breakfast" },
    { label: "Lunch", value: "Lunch" },
    { label: "Dinner", value: "Dinner" },
    { label: "Snack", value: "Snack" },
  ])



  return (
    <View style={styles.mealpicker} >

      <Text
        style={styles.text}>Meal</Text>



      <DropDownPicker
        style={styles.picker}
        textStyle={{
          fontFamily: "MontserratAlternates_400Regular"
        }}
        open={open}
        value={meal}
        items={items}
        setOpen={setOpen}
        setValue={setMeal}
        setItems={setItems}
        dropDownDirection="TOP"
        dropDownContainerStyle={{
          width: 150,
          marginLeft: 55,

        }}
      />


    </View>
  );
}

const styles = StyleSheet.create({
  text: {
    width: 100,
    fontSize: 15,
    color: "white",
    fontWeight: "bold",
    textAlign: 'left',
    fontFamily: "MontserratAlternates_800ExtraBold"
  },
  mealpicker: {
    width: 300,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 30,
    margin: 3,

  },
  picker: {
    fontFamily: "MontserratAlternates_400Regular",
    color: 'black',
    width: 150,
    marginLeft: 55,
    marginRight: 75,

  },
  


});