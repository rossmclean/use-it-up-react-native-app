import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, StyleSheet, ActivityIndicator } from 'react-native';
import Home from './Components/Home'
import SearchEngine from './Components/SearchEngine';
import Navchips from './Components/Navchips';
import Results from './Components/Results';
import AsyncStorage from '@react-native-async-storage/async-storage';
import usePrevious from './Hooks/usePrevious.js'

import {
  useFonts,
  MontserratAlternates_100Thin,
  MontserratAlternates_100Thin_Italic,
  MontserratAlternates_200ExtraLight,
  MontserratAlternates_200ExtraLight_Italic,
  MontserratAlternates_300Light,
  MontserratAlternates_300Light_Italic,
  MontserratAlternates_400Regular,
  MontserratAlternates_400Regular_Italic,
  MontserratAlternates_500Medium,
  MontserratAlternates_500Medium_Italic,
  MontserratAlternates_600SemiBold,
  MontserratAlternates_600SemiBold_Italic,
  MontserratAlternates_700Bold,
  MontserratAlternates_700Bold_Italic,
  MontserratAlternates_800ExtraBold,
  MontserratAlternates_800ExtraBold_Italic,
  MontserratAlternates_900Black,
  MontserratAlternates_900Black_Italic
} from '@expo-google-fonts/montserrat-alternates'
import MyList from './Components/MyList'








export default function App() {

  const [data, setData] = useState([])
  const [ingredientOne, setIngredientOne] = useState("")
  const [ingredientTwo, setIngredientTwo] = useState("")
  const [ingredientThree, setIngredientThree] = useState("")
  const [loading, setLoading] = useState(false)
  const [isError, setIsError] = useState(false)
  const [meal, setMeal] = useState('All')
  const [cuisine, setCuisine] = useState('All')
  const [diet, setDiet] = useState('All')
  const [displaySearch, setDisplaySearch] = useState(false)
  const [displayResults, setDisplayResults] = useState(false)
  const [displayList, setDisplayList] = useState(false)
  const [count, setCount] = useState(1)
  const [saved, setSaved] = useState([])
  const [url, setUrl] = useState("")

  console.log('url = ', url)






  const _storeData = async (data) => {
    try {
      await AsyncStorage.setItem('savedData', JSON.stringify(data));
      console.log('data stored')
    } catch (error) {
      console.log(error)
    }
  };

  const _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('savedData');
      let arraySaved = JSON.parse(value) || []
      setSaved(arraySaved)
    }
    catch (error) {
      console.log(error)
    }
  };



  useEffect(() => {
    _retrieveData()
    console.log('data loaded')

  }, [])



  if (count > 3) {
    setCount(3)
  }



  useEffect(() => {
    const errortimer = setTimeout(() => {
      setIsError(false)
      setLoading(false)
    }, 2000);
    return () => clearTimeout(errortimer);
  }, [isError]);

  let [fontsLoaded] = useFonts({
    MontserratAlternates_400Regular,
    MontserratAlternates_600SemiBold_Italic,
    MontserratAlternates_800ExtraBold_Italic,
    MontserratAlternates_800ExtraBold


  });

  if (!fontsLoaded) {
    return <ActivityIndicator size="large" color="white" />
  }







  const fetchData = async () => {
    


    if (meal === "All" && cuisine === "All" && diet !== "All") {
      debugger
      setUrl(`https://api.edamam.com/api/recipes/v2?type=public&q=${ingredientOne}%20${ingredientTwo}%20${ingredientThree}&app_id=4a3160a6&app_key=c60e8f8a4ce2975e5eb654e87c716a26&health=${diet}&random=true`)
    }

    else if (meal === "All" && cuisine === "All" && diet === "All") {

      URL = `https://api.edamam.com/api/recipes/v2?type=public&q=${ingredientOne}%20${ingredientTwo}%20${ingredientThree}&app_id=4a3160a6&app_key=c60e8f8a4ce2975e5eb654e87c716a26&random=true`


    }

    else if (meal === "All" && cuisine !== "All" && diet !== "All") {

      URL = `https://api.edamam.com/api/recipes/v2?type=public&q=${ingredientOne}%20${ingredientTwo}%20${ingredientThree}&app_id=4a3160a6&app_key=c60e8f8a4ce2975e5eb654e87c716a26&health=${diet}&cuisineType=${cuisine}&random=true`
    }

    else if (meal !== 'All' && cuisine === 'All' && diet === "All") {

      URL = `https://api.edamam.com/api/recipes/v2?type=public&q=${ingredientOne}%20${ingredientTwo}%20${ingredientThree}&app_id=4a3160a6&app_key=c60e8f8a4ce2975e5eb654e87c716a26&mealType=${meal}&random=true`

    }

    else if (cuisine !== 'All' && meal !== 'All' && diet !== 'All') {

      URL = `https://api.edamam.com/api/recipes/v2?type=public&q=${ingredientOne}%20${ingredientTwo}%20${ingredientThree}&app_id=4a3160a6&app_key=c60e8f8a4ce2975e5eb654e87c716a26&health=${diet}&cuisineType=${cuisine}&mealType=${meal}&random=true`

    }

    else if (cuisine === "All" && diet !== "All" && meal !== "All") {

      URL = `https://api.edamam.com/api/recipes/v2?type=public&q=${ingredientOne}%20${ingredientTwo}%20${ingredientThree}&app_id=4a3160a6&app_key=c60e8f8a4ce2975e5eb654e87c716a26&health=${diet}&mealType=${meal}&random=true`
    }

    else if (cuisine !== "All" && meal !== "All" && diet === "All") {
      URL = `https://api.edamam.com/api/recipes/v2?type=public&q=${ingredientOne}%20${ingredientTwo}%20${ingredientThree}&app_id=4a3160a6&app_key=c60e8f8a4ce2975e5eb654e87c716a26&cuisineType=${cuisine}&mealType=${meal}&random=true`

    }

    else if (meal === "All" && cuisine !== "All" && diet === "All") {
      URL = `https://api.edamam.com/api/recipes/v2?type=public&q=${ingredientOne}%20${ingredientTwo}%20${ingredientThree}&app_id=4a3160a6&app_key=c60e8f8a4ce2975e5eb654e87c716a26&cuisineType=${cuisine}&random=true`
    }




    try {
      setLoading(true);
      let response = await fetch(URL.replace(/ /g, "%20"))
      if (response.ok) {
        jsondata = await response.json();
        setData(jsondata.hits)

        setLoading(false)
      }
      else {
        setLoading(false)
        setIsError("Error fetching list")

      }
    } catch (error) {
      setIsError("Error fetching list")

    }
  }

  const refreshResults = async () => {
    try {
      setLoading(true);
      let response = await fetch(URL.replace(/ /g, "%20"))
      if (response.ok) {
        jsondata = await response.json();
        setData(jsondata.hits)

        setLoading(false)
      }
      else {
        setLoading(false)
        setIsError("Error fetching list")

      }
    } catch (error) {
      setIsError("Error fetching list")

    }
  }

  const handleSortNumber = () => {
    let lowHigh = data.sort((a, b) => a.recipe.ingredients.length - b.recipe.ingredients.length)
    setData([...lowHigh])
  }

  const handleSortYield = () => {
    let yieldItems = data.sort((a, b) => a.recipe.yield - b.recipe.yield)
    setData([...yieldItems])
  }

  const handleSortTime = () => {
    let sortTime = data.sort((a, b) => a.recipe.totalTime - b.recipe.totalTime)
    setData([...sortTime])
  }



  const handleSave = (item) => {
    setSaved([...saved, item])
    console.log('Item saved!')
    _storeData([...saved, item])

  }

  const handleSubmit = (e) => {
    e.preventDefault()
    setDisplayResults(true)
    setDisplaySearch(false)
    fetchData()
    // setIngredientOne("")
    // setIngredientTwo("")
    // setIngredientThree("")

  }








  return (
    <>
      <SafeAreaView
        edges={["top"]}
        style={{ flex: 0, backgroundColor: "#07BEB8" }}
      />

      <SafeAreaView
        edges={["left", "right", "bottom"]}
        style={{
          flex: 1,
          backgroundColor: "whitesmoke",
          position: "relative",
        }}
      >



        <View style={styles.container}>



          {!displaySearch && !displayResults && !displayList ? <Home displaySearch={displaySearch} setDisplaySearch={setDisplaySearch} /> : null}



          {displaySearch ? <SearchEngine
            data={data}
            isError={isError}
            loading={loading}
            displayResults={displayResults}
            setDisplaySearch={setDisplaySearch}
            setDisplayResults={setDisplayResults}
            setIngredientOne={setIngredientOne}
            setIngredientTwo={setIngredientTwo}
            setIngredientThree={setIngredientThree}
            ingredientOne={ingredientOne}
            ingredientTwo={ingredientTwo}
            ingredientThree={ingredientThree}
            count={count}
            setCount={setCount}
            meal={meal}
            setMeal={setMeal}
            cuisine={cuisine}
            setCuisine={setCuisine}
            handleSubmit={handleSubmit}
            saved={saved}
            handleSave={handleSave}
            handleSortNumber={handleSortNumber}
            displaySearch={displaySearch}
            diet={diet}
            setDiet={setDiet}

          /> : null}

          {displayResults ? <Results
            setDisplayResults={setDisplayResults}
            handleSortNumber={handleSortNumber}
            handleSortYield={handleSortYield}
            handleSortTime={handleSortTime}
            loading={loading}
            isError={isError}
            data={data}
            saved={saved}
            handleSave={handleSave}
            meal={meal}
            cuisine={cuisine}
            diet={diet}
            fetchData={fetchData}
            ingredientOne={ingredientOne}
            refreshResults={refreshResults}

          /> : null}

          {displayList ? <MyList saved={saved} setSaved={setSaved} _storeData={_storeData} /> : null}


        </View>


        <Navchips setDisplaySearch={setDisplaySearch} setDisplayResults={setDisplayResults} setDisplayList={setDisplayList} setIngredientOne={setIngredientOne} setIngredientTwo={setIngredientTwo} setIngredientThree={setIngredientThree} />





      </SafeAreaView>
    </>



  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: "#07BEB8"
  },
  box: {
    width: '100%',
    height: 200,
  },
  item: {
    width: '90%',
    flexDirection: "column",
    alignItems: 'center',
    borderWidth: 2,
    backgroundColor: 'white',
    padding: 10,
    marginVertical: 16,
    marginHorizontal: 10,
  },
  title: {
    fontSize: 20,
    paddingTop: 5,

  },
  input: {
    height: 40,
    width: 200,
    margin: 12,
    borderWidth: 2,
    padding: 10,
    borderRadius: 10,
  },
  smallimg: {
    width: 150,
    height: 120
  },
  text: {
    fontSize: 10,
    color: "black",
    fontWeight: "bold",
    textAlign: 'center',
  },
  button: {
    width: 100,
    padding: 20,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: 'white',
  },
  addremovebutton: {
    marginRight: 10,
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 100,
    backgroundColor: 'orange',
  },
  types: {
    borderRadius: 15,
    padding: 5,
    margin: 5,
    backgroundColor: "orange",
    fontSize: 12,
    color: 'black',


  }
});